(set-env!
 :source-paths #{"src" "content"}
 :dependencies '[[perun "0.4.2-SNAPSHOT" :scope "test"]
                 [hiccup "1.0.5" :exclusions [org.clojure/clojure]]])

(require '[io.perun :as p])

(task-options!
  pom {:project 'com.inquisitivenonsense
       :version "0.0.1"}
  p/render {:renderer 'com.inquisitivenonsense.core/page})

(deftask build []
  (comp (p/markdown)
        (p/render)
	(target)))
